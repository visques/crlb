\documentclass[twocolumn,10pt]{IEEEtran}
\usepackage{array}
\usepackage{cite}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsxtra}
\usepackage{varioref}
\usepackage{float}
\usepackage{rotate}
\usepackage{color}
\usepackage{enumerate}
\usepackage[active]{srcltx}
\usepackage{mathrsfs}
\usepackage[dvipsnames]{xcolor}
\usepackage{rotating}
\usepackage{times}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{fontenc}
\usepackage{transparent}
\usepackage{graphicx}
\usepackage[colorlinks=false,linkcolor=black,citecolor=black,hidelinks]{hyperref}
\usepackage{verbatim}
\usepackage{hhline}
%\usepackage{subfigure}
\usepackage[caption = true]{subfig}
%\usepackage{epstopdf}

\begin{document}
\title{Cotas de Cramer-Rao para distribuciones Ricianas y TWDP}

\author{Guillermo~Castro}
\maketitle

\begin{abstract}
Se determinan y grafican las cotas de Cramer-Rao para varianza de estimadores de parámetros Ricianos y TWDP
\end{abstract}

\section{Introducción}

La cota de Cramer-Rao límita inferiormente la varianza de cualquier estimador de parámetros insesgado que pueda construirse dada cierta distribución (en particular su función de densidad de probabilidad). Cualquier estimador insesgado tendrá varianza igual o superior a aquella indicada por la cota o, dicho de otro modo, un estimador insesgado perfecto tendrá varianza igual a la cota de Cramer-Rao. 

Su uso se ha extendido ampliamente en la literatura para comparar diferentes estimadores de un mismo parámetro o para demostrar que tan buen comportamiento tiene cualquier estimador propuesto.

A continuación se explica brevemente la forma de obtener este límite de varianza para casos de uno y múltiples parámetros. Posteriormente se revisa el caso particular riciano, disponible en la literatura, y el caso Two Wave Diffuse Power (TWDP) sin conocimiento \emph{a priori}, que según revisión bibliográfica de quien escribe nunca ha sido calculado.

\section{Cota Inferior de Cramer-Rao y Matriz de Información de Fisher}

\subsection{Caso de un parámetro}

Sea $T(\mathbf{x})$ un estimador del parámetro $\theta$ basado en muestras de las variables aleatorias $\mathbf{x}_1$, $\mathbf{x}_1$, $\dots$, $\mathbf{x}_n$ con densidad de probabilidad conjunta $f(\mathbf{x};\theta)$. Si $T(\mathbf{x})$ es insesgado, es decir $\mathrm{E}[T(\mathbf{x})]=\theta$, y se cumplen ciertas condiciones de regularidad entonces \cite{Papoulis2002}:

\begin{equation}
\label{eq:crlb}
\mathrm{Var}[T(\mathbf{x})] \geq \frac{1}{\mathrm{E}\left\{ \left( \frac{\partial}{\partial \theta} \log f(\mathbf{x};\theta) \right)^2 \right\}}
\end{equation}, donde $\mathrm{E}[\cdot]$ denota el operador esperanza.

Además,

\begin{equation}
\label{eq:cr-fisher}
\frac{1}{\mathrm{E}\left\{ \left( \frac{\partial}{\partial \theta} \log f(\mathbf{x};\theta) \right)^2 \right\}} = \frac{-1}{\mathrm{E}\left\{ \frac{\partial^2}{\partial \theta^2} \log f(\mathbf{x};\theta) \right\}} = \mathbf{F}_{11}^{-1}
\end{equation}, en donde $\mathbf{F}_{11}$ corresponde a lo que se denomina la información de Fisher contenida en el conjunto de datos sobre el parámetro $\theta$.

\subsection{Caso de múltiples parámetros}

En general, la matriz de información de Fisher $\mathbf{F}$ para una distribución con vector de parámetros $\boldsymbol{\theta} = \theta_1$, $\theta_2$, $\dots$, $\theta_m$ tendrá la siguiente forma:

\begin{equation}
\mathbf{F} = \begin{bmatrix}
\mathbf{F}_{11} & \mathbf{F}_{12} & \dots & \mathbf{F}_{1m}\\
\mathbf{F}_{21} & \mathbf{F}_{22} & \dots & \mathbf{F}_{2m}\\
\vdots & \vdots & \ddots & \vdots\\
\mathbf{F}_{m1} & \mathbf{F}_{m2} & \dots & \mathbf{F}_{mm}
\end{bmatrix}
\end{equation}

Y sus elementos están definidos como

\begin{equation}
\mathbf{F}_{ij} = -\mathrm{E}\left\{ \frac{\partial^2}{\partial \theta_i\theta_j} \log f(\mathbf{x};\boldsymbol{\theta}) \right\}
\end{equation}

Usando \eqref{eq:cr-fisher} se pueden reescribir los elementos $\mathbf{F}_{ij}$ como muestra \eqref{eq:Jsimple}:

\begin{equation}
\label{eq:Jsimple}
\mathbf{F}_{ij} = \begin{cases}
			\mathrm{E}\left\{ \left( \frac{\partial}{\partial \theta_i} \log f(\mathbf{x};\boldsymbol{\theta}) \right)^2 \right\} & \quad \text{si } i = j\\
			\mathrm{E}\left\{ \frac{\partial}{\partial \theta_i} \log f(\mathbf{x};\boldsymbol{\theta})\frac{\partial}{\partial \theta_j} \log f(\mathbf{x};\boldsymbol{\theta}) \right\}  & \quad \text{si } i \neq j
		\end{cases}
\end{equation}

Una vez construida $\mathbf{F}$ se replantea el resultado de Cramer-Rao como:

\begin{equation}
\mathrm{Cov}_{\boldsymbol{\theta}} \geq \mathbf{C} = \mathbf{F}^{-1}
\end{equation}

En donde $\mathbf{C}$ es una matriz de elementos $\mathbf{C}_{ij}$ que contiene en su diagonal principal las cotas de estimación para cada parámetro al usar algún estimador insesgado $T(\mathbf{x})$, es decir:

\begin{align}
	\mathrm{Var_{\theta_1}}[T(\mathbf{x})] &\geq \mathbf{C}_{11}\\
	\mathrm{Var_{\theta_2}}[T(\mathbf{x})] &\geq \mathbf{C}_{22}\\
	\vdots &\\
	\mathrm{Var_{\theta_m}}[T(\mathbf{x})] &\geq \mathbf{C}_{mm}
\end{align}

\subsection{Caso de variables aleatorias i.i.d. e Interpretacíon}

Si las variables aleatorias $\mathbf{x}_1,\mathbf{x}_2,\dots,\mathbf{x}_n$ son $i.i.d.$ es posible escribir su función de densidad de probabilidad conjunta como:

\begin{equation}
	f(\mathbf{x};\theta) = f(x;\theta)^n
\end{equation}, en donde $f(x;\theta)$ es la función de densidad de probabilidad de las variables aleatorias $i.i.d.$ $\mathbf{x}_1,\mathbf{x}_2,\dots,\mathbf{x}_n$.

Así,

\begin{equation}
	\log f(\mathbf{x};\boldsymbol{\theta}) = n\log f(x;\boldsymbol{\theta})
\end{equation}

Por lo tanto, para el caso de estimación escalar y muestras $i.i.d.$:

\begin{equation}
\label{eq:crlbiid}
\mathrm{Var}[T(\mathbf{x})] \geq \frac{1}{n\mathrm{E}\left\{ \left( \frac{\partial}{\partial \theta} \log f(x;\theta) \right)^2 \right\}}
\end{equation}

Mientras que para el caso vectorial de muestras $i.i.d.$:

\begin{equation}
\label{eq:Jsimple}
\mathbf{F}_{ij} = \begin{cases}
n\mathrm{E}\left\{ \left( \frac{\partial}{\partial \theta_i} \log f(x;\boldsymbol{\theta}) \right)^2 \right\} & \quad \text{si } i = j\\
n\mathrm{E}\left\{ \frac{\partial}{\partial \theta_i} \log f(x;\boldsymbol{\theta})\frac{\partial}{\partial \theta_j} \log f(x;\boldsymbol{\theta}) \right\}  & \quad \text{si } i \neq j
\end{cases}
\end{equation}


De esta manera, si se requiere de una cota de desempeño para un estimador de parámetros se debe revisar primero si él o los parámetros siendo estimados corresponden a la totalidad de parámetros de la distribución o si existen algunos que se conocen o se asumen dados. Si lo único desconocido es el parámetro que se está estimando se puede realizar el cálculo de la cota en su versión escalar. Si hay más de un parámetro desconocido se debe hacer el cálculo en su versión vectorial mediante la matriz de información de Fisher. En distribuciones multiparámetro es común ver cotas calculadas de manera escalar por simplicidad de análisis. Un caso típico de esto es la distribución riciana pues las cotas calculadas escalar y vectorialmente difieren de manera despreciable \cite{Tepedelenlioglu2003}. Es decir, el concoer o no conocer el parámetro de potencia promedio $\Omega$ impacta muy poco en la varianza de estimación del factor de forma $K$. En la siguiente sección se muestra el desarrollo típico de este problema.


\section{Cotas de Cramer Rao para distribucion riciana}

La función de densidad de probabilidad riciana puede escribirse como \cite{Tepedelenlioglu2003}:

\begin{multline}
	f_{R}(r) = \frac{2(K+1)r}{\Omega}\exp\left( -\frac{(K+1)r^2}{\Omega}-K \right) \\
	\cdot\mathbf{I}_0\left( 2r\sqrt{\frac{K(K+1)}{\Omega}} \right)
\end{multline}

\subsection{Caso $\Omega$ conocido}

Si se conoce o asume conocido $\Omega$, se puede calcular la cota de Cramer-Rao de estimadores de $K$ para una muestra de $n$ valores independientes como \cite{Tepedelenlioglu2003}:

\begin{equation}
\label{eq:crlbiid}
\mathrm{CRB_{K|\Omega}}(K) = \frac{1}{n\mathrm{E}\left\{ \left( \frac{\partial}{\partial K} \log f_R(r) \right)^2 \right\}}
\end{equation}


\begin{multline}
	\frac{\partial}{\partial K} \log f_R(r) = \frac{1}{K+1}-1-\frac{r^2}{\Omega}\\+\frac{\mathbf{I}_1\left( 2r\sqrt{\frac{K(K+1)}{\Omega}} \right)}{\mathbf{I}_0\left( 2r\sqrt{\frac{K(K+1)}{\Omega}} \right)}\frac{2K+1}{\sqrt{\Omega K(K+1)}}r
\end{multline}

\subsection{Caso $\Omega$ desconocido}

En el caso en el que $\Omega$ es desconocido, se debe calcular la matriz $\mathbf{C}$, para lo que resta encontrar $\frac{\partial}{\partial \Omega} \log f_R(r)$ y calcular $\mathbf{F}^{-1}$:

\begin{multline}
	\frac{\partial}{\partial \Omega} \log f_R(r) = -\frac{1}{\Omega} + \frac{(K+1)r^2}{\Omega^2}\\-\frac{\mathbf{I}_1\left( 2r\sqrt{\frac{K(K+1)}{\Omega}} \right)}{\mathbf{I}_0\left( 2r\sqrt{\frac{K(K+1)}{\Omega}} \right)}\sqrt{\frac{K(K+1)}{\Omega^3}}r
\end{multline}

\begin{equation}
\mathbf{F} = n\begin{bmatrix}
	\mathrm{E}\left\{ \left( \frac{\partial}{\partial K} \log f(r) \right)^2 \right\} & \mathrm{E}\left\{ \frac{\partial}{\partial K} \log f(r)\frac{\partial}{\partial \Omega} \log f(r) \right\}\\
	\mathrm{E}\left\{ \frac{\partial}{\partial \Omega} \log f(r)\frac{\partial}{\partial K} \log f(r) \right\} & \mathrm{E}\left\{ \left( \frac{\partial}{\partial \Omega} \log f(r) \right)^2 \right\}
\end{bmatrix}
\end{equation}

Recordando que $\mathbf{C}=\mathbf{F}^{-1}$, se puede escribir:

\begin{align}
	CRB_{K}(K) = \mathbf{C}_{11}\\
	CRB_{\Omega}(\Omega) = \mathbf{C}_{22}
\end{align}

\begin{figure}
{\includegraphics[width=1\columnwidth]{img/Rician.pdf}}
\caption{Cotas de Cramer-Rao para estimadores de factor $K$ con o sin conocimiento de potencia media $\Omega$.}
\label{fig:ricianCRLB}
\end{figure}

En Fig. \ref{fig:ricianCRLB} se muestran las gráficas de $CRB_{K|\Omega}(K)$ y $CRB_{K}(K)$. Tal como se mencionó anteriormente y como se muestra en \cite{Tepedelenlioglu2003}, el conocimiento del parámetro $\Omega$ no mejora de manera significativa el desempeño máximo que algún estimador de $K$ puede lograr. La curva $CRB_{K1\Omega}(K)$ está siempre por debajo de aquella de $CRB_{K}(K)$ o, dicho equivalentemente, la estimación de $K$ realizada en conocimiento del parámetro $\Omega$ será siempre más precisa que aquella en que se desconoce.

\section{Cotas de Cramer-Rao para distribución TWDP en desconocimiento total de parámetros}

En este caso la funciónde densidad de probabilidad puede escibirse como \cite{Durgin2002,Rao2015}:

\begin{equation}
\label{eq:twdpfdp}
f_T(r) = \frac{2(K+1)r}{\pi\Omega}\mathrm{e}^{-\frac{(K+1)r^2}{\Omega}-K}\int_{0}^{\pi} \mathrm{e}^{\Delta K \cos{\theta}} \mathrm{I}_0 \left( \bar{r} \right) d\theta
\end{equation}

con

\begin{equation}
\bar{r} = 2r\sqrt{\frac{K(K+1)(1-\Delta\cos{\theta})}{\Omega}}
\end{equation}

La función de densidad de probabilidad TWDP queda totalemente definida por tres parámetros llamados $K$, $\Delta$, y $\Omega$, por lo que la matriz de información de Fisher a calcular tiene dimensión $3\mathbf{x}3$, compuesta por los elementos:

\begin{align}
\nonumber
\mathbf{F}_{11} &= \mathrm{E}\left[\left(\frac{\partial\ln{f_T(r)}}{\partial K}\right)^2\right]\\
\mathbf{F}_{12} &= \mathbf{F}_{21} = \mathrm{E}\left[\frac{\partial\ln{f_T(r)}}{\partial K}\frac{\partial\ln{f_T(r)}}{\partial\Delta}\right]\\
\mathbf{F}_{13} &= \mathbf{F}_{31} = \mathrm{E}\left[\frac{\partial\ln{f_T(r)}}{\partial K}\frac{\partial\ln{f_T(r)}}{\partial\Omega}\right]\\
\mathbf{F}_{22} &= \mathrm{E}\left[\left(\frac{\partial\ln{f_T(r)}}{\partial\Delta}\right)^2\right]\\
\mathbf{F}_{23} &= \mathbf{F}_{32} = \mathrm{E}\left[\frac{\partial\ln{f_T(r)}}{\partial\Delta}\frac{\partial\ln{f_T(r)}}{\partial\Omega}\right]\\
\mathbf{F}_{33} &= \mathrm{E}\left[\left(\frac{\partial\ln{f_T(r)}}{\partial\Omega}\right)^2\right]
\end{align}

en donde

\begin{multline}
\frac{\partial \ln{f_T(r)}}{\partial K} = \frac{1}{K+1}-\frac{r^2}{\Omega}-1\\
+\frac{\int_{0}^{\pi} \Delta\cos{\theta}\mathrm{e}^{\Delta K \cos{\theta}} \mathrm{I}_0 \left( \bar{r} \right) d\theta}{\int_{0}^{\pi} \mathrm{e}^{\Delta K \cos{\theta}} \mathrm{I}_0 \left( \bar{r} \right) d\theta}\\
+\frac{\int_{0}^{\pi} \mathrm{e}^{\Delta K \cos{\theta}} \mathrm{I}_1 \left( \bar{r} \right) \frac{(2K+1)(1-\Delta\cos{\theta})r}{\sqrt{K\Omega(K+1)(1-\Delta\cos{\theta})}} d\theta}{\int_{0}^{\pi} \mathrm{e}^{\Delta K \cos{\theta}} \mathrm{I}_0 \left( \bar{r} \right) d\theta}
\end{multline}

\begin{multline}
\frac{\partial \ln{f_T(r)}}{\partial \Delta} =\frac{\int_{0}^{\pi} K \cos{\theta}\mathrm{e}^{\Delta K \cos{\theta}} \mathrm{I}_0 \left( \bar{r} \right) d\theta}{\int_{0}^{\pi} \mathrm{e}^{\Delta K \cos{\theta}} \mathrm{I}_0 \left( \bar{r} \right) d\theta}\\
-\frac{\int_{0}^{\pi} \mathrm{e}^{\Delta K \cos{\theta}} \mathrm{I}_1 \left( \bar{r} \right) \frac{K(K+1)\cos{\theta}r}{\sqrt{K\Omega(K+1)(1-\Delta\cos{\theta})}} d\theta}{\int_{0}^{\pi} \mathrm{e}^{\Delta K \cos{\theta}} \mathrm{I}_0 \left( \bar{r} \right) d\theta}
\end{multline}

\begin{multline}
\frac{\partial \ln{f_T(r)}}{\partial \Omega} = -\frac{1}{\Omega}+\frac{(K+1)r^2}{\Omega^2}\\
-\frac{\int_{0}^{\pi} \mathrm{e}^{\Delta K \cos{\theta}} \mathrm{I}_1 \left( \bar{r} \right) \frac{\sqrt{K\Omega(K+1)(1-\Delta\cos{\theta})}}{\Omega^2}r d\theta}{\int_{0}^{\pi} \mathrm{e}^{\Delta K \cos{\theta}} \mathrm{I}_0 \left( \bar{r} \right) d\theta}
\end{multline}

Con esta información es posible obtener la matriz $\mathbf{C}=\mathbf{F}^{-1}$, en donde aparecen:

\begin{align}
	CRB_{K}(K) &= \mathbf{C}_{11}\\
	CRB_{\Delta}(\Delta) &= \mathbf{C}_{22}\\
	CRB_{\Omega}(\Omega) &= \mathbf{C}_{33}
\end{align}


\begin{figure*}[ht]
\subfloat[N = 1000\label{fig:guille1000}]{\includegraphics[width=0.32\textwidth]{img/TWDP_1.pdf}}
\subfloat[N = 10000\label{fig:guille10000}]{\includegraphics[width=0.32\textwidth]{img/TWDP_2.pdf}}
\subfloat[N = 100000\label{fig:guille100000}]{\includegraphics[width=0.32\textwidth]{img/TWDP_3.pdf}}\\
\subfloat[N = 1000\label{fig:lopez1000}]{\includegraphics[width=0.32\textwidth]{img/TWDP_4.pdf}}
\subfloat[N = 10000\label{fig:lopez10000}]{\includegraphics[width=0.32\textwidth]{img/TWDP_5.pdf}}
\subfloat[N = 100000\label{fig:lopez100000}]{\includegraphics[width=0.32\textwidth]{img/TWDP_6.pdf}}
\caption{.}
\label{fig:crbtwdp}
\end{figure*}

Fig. \ref{fig:crbtwdp}

%\begin{figure*}

%\subfloat[N = 1000\label{fig:guille1000}]{\includegraphics[width=0.32\textwidth]{img/Guille1000.pdf}}
%\subfloat[N = 10000\label{fig:guille10000}]{\includegraphics[width=0.32\textwidth]{img/Guille10000.pdf}}
%\subfloat[N = 100000\label{fig:guille100000}]{\includegraphics[width=0.32\textwidth]{img/Guille100000.pdf}}\\
%\subfloat[N = 1000\label{fig:lopez1000}]{\includegraphics[width=0.32\textwidth]{img/Trampa1000.pdf}}
%\subfloat[N = 10000\label{fig:lopez10000}]{\includegraphics[width=0.32\textwidth]{img/Trampa10000.pdf}}
%\subfloat[N = 100000\label{fig:lopez100000}]
%{\includegraphics[width=0.32\textwidth]{img/Trampa100000.pdf}}
%\caption{$(\hat{K}, \hat{\Delta})$ mean (blue dots) and standard deviation (blue bars) using proposed method (top) and Lopez' estimator (bottom) and true $(K, \Delta)$ pairs over the $KD > 2$ region (black boundary).}
%\label{fig:kdplane}
%\end{figure*}

%\section{Conclusion}
%
%\appendix[Cramer-Rao bound for TWDP envelope estimators]

\bibliographystyle{IEEEtran}
\bibliography{TWDP}

\end{document}
